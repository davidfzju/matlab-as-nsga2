function popm=Mutate(pop,lb,ub,mutate_rand)
    nx=numel(pop);
    change=randperm(nx,1);%随机得到变异点位
    popm=pop;
    % if change==1
    %     popm(1,change)=x1min+(x1max-x1min).*rand();
    % elseif change==2
    %     popm(1,change)=x2min+(x2max-x2min).*rand();
    % elseif change==3
    %     popm(1,change)=x3min+(x3max-x3min).*rand();
    % end
    popm(1,change) = lb(change)+(ub(change)-lb(change))*mutate_rand;
end